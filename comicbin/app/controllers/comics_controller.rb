class ComicsController < ApplicationController

  def index
    @comics = Comic.all
    # render 'index'
  end

  def show
    @comic = Comic.find_by :id => params[:id]
    render 'show'
  end

  def destroy
    @comicToDestroy = Comic.find_by :id => params[:id]
    @comicToDestroy.destroy
    redirect_to "/comics"
  end

  def new
    @comic = Comic.new
    render 'new'
  end

  def create
   @comic = Comic.new
   @comic.title = params[:comic][:title]
   @comic.description = params[:comic][:description]
   @comic.imageURL = params[:comic][:imageURL]
    if @comic.save
      redirect_to comics_path
    else
      render :action => 'new'
    end

  end
end
